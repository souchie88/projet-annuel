<?php

namespace App\Repository;

use App\Entity\Stockages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Stockages>
 *
 * @method Stockages|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stockages|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stockages[]    findAll()
 * @method Stockages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stockages::class);
    }

    //    /**
    //     * @return Stockages[] Returns an array of Stockages objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Stockages
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
