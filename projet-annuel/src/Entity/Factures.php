<?php

namespace App\Entity;

use App\Repository\FacturesRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FacturesRepository::class)]
class Factures
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'DateFacture')]
    private ?Utilisateurs $IdUtilisateur = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $DateFacture = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $DesignationFacture = null;

    #[ORM\Column]
    private ?float $Prix = null;

    #[ORM\Column]
    private ?int $Quantite = null;

    #[ORM\Column]
    private ?float $PrixTotal = null;

    #[ORM\Column]
    private ?float $MontantTVA = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUtilisateur(): ?Utilisateurs
    {
        return $this->IdUtilisateur;
    }

    public function setIdUtilisateur(?Utilisateurs $IdUtilisateur): static
    {
        $this->IdUtilisateur = $IdUtilisateur;

        return $this;
    }

    public function getDateFacture(): ?\DateTimeInterface
    {
        return $this->DateFacture;
    }

    public function setDateFacture(\DateTimeInterface $DateFacture): static
    {
        $this->DateFacture = $DateFacture;

        return $this;
    }

    public function getDesignationFacture(): ?string
    {
        return $this->DesignationFacture;
    }

    public function setDesignationFacture(?string $DesignationFacture): static
    {
        $this->DesignationFacture = $DesignationFacture;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->Prix;
    }

    public function setPrix(float $Prix): static
    {
        $this->Prix = $Prix;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->Quantite;
    }

    public function setQuantite(int $Quantite): static
    {
        $this->Quantite = $Quantite;

        return $this;
    }

    public function getPrixTotal(): ?float
    {
        return $this->PrixTotal;
    }

    public function setPrixTotal(float $PrixTotal): static
    {
        $this->PrixTotal = $PrixTotal;

        return $this;
    }

    public function getMontantTVA(): ?float
    {
        return $this->MontantTVA;
    }

    public function setMontantTVA(float $MontantTVA): static
    {
        $this->MontantTVA = $MontantTVA;

        return $this;
    }
}
