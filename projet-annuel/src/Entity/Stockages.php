<?php

namespace App\Entity;

use App\Repository\StockagesRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StockagesRepository::class)]
class Stockages
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'EspaceStockage')]
    private ?Utilisateurs $IdUtilisateur = null;

    #[ORM\Column]
    private ?int $EspaceStockage = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $DateAchat = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUtilisateur(): ?Utilisateurs
    {
        return $this->IdUtilisateur;
    }

    public function setIdUtilisateur(?Utilisateurs $IdUtilisateur): static
    {
        $this->IdUtilisateur = $IdUtilisateur;

        return $this;
    }

    public function getEspaceStockage(): ?int
    {
        return $this->EspaceStockage;
    }

    public function setEspaceStockage(int $EspaceStockage): static
    {
        $this->EspaceStockage = $EspaceStockage;

        return $this;
    }

    public function getDateAchat(): ?\DateTimeInterface
    {
        return $this->DateAchat;
    }

    public function setDateAchat(\DateTimeInterface $DateAchat): static
    {
        $this->DateAchat = $DateAchat;

        return $this;
    }
}
