<?php

namespace App\Entity;

use App\Repository\FichiersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FichiersRepository::class)]
class Fichiers
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $NomFichier = null;

    #[ORM\Column]
    private ?float $TailleFichier = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $DateUpload = null;

    #[ORM\ManyToOne]
    private ?Stockages $IdStockages = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomFichier(): ?string
    {
        return $this->NomFichier;
    }

    public function setNomFichier(string $NomFichier): static
    {
        $this->NomFichier = $NomFichier;

        return $this;
    }

    public function getTailleFichier(): ?float
    {
        return $this->TailleFichier;
    }

    public function setTailleFichier(float $TailleFichier): static
    {
        $this->TailleFichier = $TailleFichier;

        return $this;
    }

    public function getDateUpload(): ?\DateTimeInterface
    {
        return $this->DateUpload;
    }

    public function setDateUpload(\DateTimeInterface $DateUpload): static
    {
        $this->DateUpload = $DateUpload;

        return $this;
    }

    public function getIdStockages(): ?Stockages
    {
        return $this->IdStockages;
    }

    public function setIdStockages(?Stockages $IdStockages): static
    {
        $this->IdStockages = $IdStockages;

        return $this;
    }
}
