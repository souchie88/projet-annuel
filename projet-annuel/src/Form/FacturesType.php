<?php

namespace App\Form;

use App\Entity\Factures;
use App\Entity\Utilisateurs;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FacturesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('DateFacture')
            ->add('DesignationFacture')
            ->add('Prix')
            ->add('Quantite')
            ->add('PrixTotal')
            ->add('MontantTVA')
            ->add('IdUtilisateur', EntityType::class, [
                'class' => Utilisateurs::class,
'choice_label' => 'id',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Factures::class,
        ]);
    }
}
