<?php

namespace App\DataFixtures;

use App\Entity\Utilisateurs;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UtilisateursFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $utilisateur = new Utilisateurs();
        $utilisateur->setEmail('admin@gmail.com');
        // Hashage du mot de passe avant de le définir
        $password = $this->passwordHasher->hashPassword($utilisateur, 'admin');
        $utilisateur->setPassword($password);
        $utilisateur->setPrenom('prenom');
        $utilisateur->setNom('nom');
        $utilisateur->setIsVerified(true);
        $utilisateur->setRoles(['ROLE_ADMIN']);
        $utilisateur->setRueClient('rueAdmin');
        $utilisateur->setVilleClient('villeAdmin');
        $utilisateur->setCodePostalClient(99999);

        // Persiste l'objet et le sauvegarde dans la base de données
        $manager->persist($utilisateur);

        // Exécute toutes les requêtes persistées
        $manager->flush();
    }
}
