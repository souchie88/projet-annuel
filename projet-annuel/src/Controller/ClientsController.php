<?php

namespace App\Controller;

use App\Entity\Utilisateurs;
use App\Entity\Factures;
use App\Entity\Fichiers;
use App\Entity\Stockages;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ClientsController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/clients', name: 'app_clients')]
    public function index(): Response
    {
        return $this->render('clients/index.html.twig', [
            'controller_name' => 'ClientsController',
        ]);
    }

    #[Route('/update-information', name: 'app_update_user_information', methods: ['POST'])]
    public function updateInformation(Request $request): Response
    {
        $user = $this->getUser();

        // Mettre à jour les informations de l'utilisateur
        $rueClient = $request->request->get('rueClient');
        if ($rueClient !== null) {
            $user->setRueClient($rueClient);
        }

        $villeClient = $request->request->get('villeClient');
        if ($villeClient !== null) {
            $user->setVilleClient($villeClient);
        }

        $codePostalClient = $request->request->get('codePostalClient');
        if ($codePostalClient !== null) {
            $user->setCodePostalClient($codePostalClient);
        }

        // Enregistrer les modifications
        $this->entityManager->flush();

        // Rediriger vers la page des informations de l'utilisateur
        return $this->redirectToRoute('app_clients');
    }

    #[Route('/delete-user', name: 'app_delete_user', methods: ['POST'])]
    public function deleteUser(Request $request): RedirectResponse
    {
        $user = $this->getUser();
        $entityManager = $this->entityManager;

        // Récupérer les factures liées à l'utilisateur
        $factures = $entityManager->getRepository(Factures::class)->findBy(['IdUtilisateur' => $user]);

        // Supprimer les factures
        foreach ($factures as $facture) {
            $entityManager->remove($facture);
        }

        // Récupérer les fichiers liés aux stockages de l'utilisateur
        $stockages = $entityManager->getRepository(Stockages::class)->findBy(['IdUtilisateur' => $user]);
        foreach ($stockages as $stockage) {
            $fichiers = $entityManager->getRepository(Fichiers::class)->findBy(['IdStockages' => $stockage]);
            foreach ($fichiers as $fichier) {
                $entityManager->remove($fichier);
            }
            $entityManager->remove($stockage);
        }

        // Supprimer l'utilisateur
        $entityManager->remove($user);
        $entityManager->flush();

        // Redirection ou autre action après la suppression de l'utilisateur
        return $this->redirectToRoute('app_login');
    }
}
