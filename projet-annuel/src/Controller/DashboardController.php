<?php

namespace App\Controller;

use App\Entity\Stockages;
use App\Entity\Factures;
use App\Entity\Fichiers;
use App\Repository\StockagesRepository;
use App\Repository\FichiersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class DashboardController extends AbstractController
{
    private $stockagesRepository;
    private $fichiersRepository;
    private $entityManager;

    public function __construct(StockagesRepository $stockagesRepository, FichiersRepository $fichiersRepository, EntityManagerInterface $entityManager)
    {
        $this->stockagesRepository = $stockagesRepository;
        $this->fichiersRepository = $fichiersRepository;
        $this->entityManager = $entityManager;
    }

    #[Route('/dashboard', name: 'app_dashboard')]
    public function index(Request $request): Response
    {
        $user = $this->getUser();

        if (!$user) {
            return $this->redirectToRoute('app_login');
        }

        $stockages = $this->stockagesRepository->findBy(['IdUtilisateur' => $user]);

        // Récupérer les fichiers pour chaque espace de stockage
        $filesByStockage = [];
        foreach ($stockages as $stockage) {
            $files = $this->fichiersRepository->findBy(['IdStockages' => $stockage]);
            $filesByStockage[$stockage->getId()] = $files;
        }

        foreach ($stockages as $stockage) {
            $form = $this->createFormBuilder()
                ->add('additionalSpace', IntegerType::class, [
                    'label' => 'Ajouter de l\'espace de stockage (MB)',
                    'constraints' => [new NotBlank(), new Positive()],
                ])
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $additionalSpace = $form->get('additionalSpace')->getData();

                // Ajouter l'espace de stockage
                $stockage->setEspaceStockage($stockage->getEspaceStockage() + $additionalSpace);

                // Enregistrer les modifications
                $this->entityManager->flush();

                // Rafraîchir la page pour afficher les changements
                return $this->redirectToRoute('app_dashboard');
            }

            $stockage->additionalSpaceForm = $form->createView();
        }

        return $this->render('dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
            'stockages' => $stockages,
            'filesByStockage' => $filesByStockage, // Passer les fichiers à la vue
        ]);
    }

    #[Route('/dashboard/{id}/add-space', name: 'app_dashboard_add_space', methods: ['POST'])]
    public function addSpace(Request $request, Stockages $stockage): Response
    {
        $additionalSpace = (int) $request->request->get('additionalSpace', 0);

        if ($additionalSpace <= 0) {
            $this->addFlash('error', 'Invalid additional space value.');
            return $this->redirectToRoute('app_dashboard');
        }

        // Ajouter l'espace de stockage
        $stockage->setEspaceStockage($stockage->getEspaceStockage() + $additionalSpace);

        // Créer une nouvelle facture
        $facture = new Factures();
        $facture->setIdUtilisateur($this->getUser());
        $facture->setDateFacture(new \DateTime());
        $facture->setDesignationFacture("Ajout d'espace de stockage");
        $facture->setPrix(1);
        $facture->setQuantite($additionalSpace);
        $facture->setPrixTotal(0.001 * $additionalSpace);
        $facture->setMontantTVA(0.20);

        // Persist et flush les changements
        $this->entityManager->persist($facture);
        $this->entityManager->flush();

        $this->addFlash('success', 'Additional space added successfully.');
        return $this->redirectToRoute('app_dashboard');
    }

    #[Route('/dashboard/upload-file', name: 'app_dashboard_upload_file', methods: ['POST'])]
    // Étape 1 : Stockage du fichier dans un dossier temporaire
    public function uploadFile(Request $request): Response
    {
        $user = $this->getUser();

        // Vérifier si un utilisateur est connecté
        if (!$user) {
            return $this->redirectToRoute('app_login');
        }

        // Récupérer le fichier téléchargé
        $uploadedFile = $request->files->get('file');

        // Vérifier si un fichier a été téléchargé
        if (!$uploadedFile instanceof UploadedFile) {
            $this->addFlash('error', 'No file uploaded.');
            return $this->redirectToRoute('app_dashboard');
        }

        // Récupérer l'id de l'utilisateur
        $userId = $user->getId();

        // Définir le dossier de destination spécifique à l'utilisateur
        $destination = $this->getParameter('upload_directory') . '/' . $userId;

        // Créer le dossier de destination s'il n'existe pas déjà
        if (!file_exists($destination)) {
            mkdir($destination, 0777, true);
        }

        // Générer un nom de fichier unique pour éviter les conflits
        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        $newFilename = $originalFilename . '.' . $uploadedFile->guessExtension();

        // Déplacer le fichier vers le dossier de destination
        try {
            $uploadedFile->move($destination, $newFilename);
        } catch (FileException $e) {
            $this->addFlash('error', 'File upload failed.');
            return $this->redirectToRoute('app_dashboard');
        }

        // Rediriger vers l'étape 2 pour enregistrer les détails du fichier
        return $this->redirectToRoute('app_dashboard_save_file', [
            'userId' => $userId,
            'filename' => $newFilename,
        ]);
    }

    #[Route('/dashboard/save-file/{userId}/{filename}', name: 'app_dashboard_save_file')]
    public function saveFileDetails(Request $request, int $userId, string $filename): Response
    {
        // Récupérer l'utilisateur connecté
        $user = $this->getUser();

        // Vérifier si l'utilisateur est connecté et si son ID correspond à celui fourni dans l'URL
        if (!$user || $user->getId() !== $userId) {
            $this->addFlash('error', 'Unauthorized access.');
            return $this->redirectToRoute('app_dashboard');
        }

        // Vérifier si le fichier existe déjà pour cet utilisateur
        $existingFile = $this->fichiersRepository->findOneBy(['NomFichier' => $filename]);
        if ($existingFile) {
            $this->addFlash('error', 'File already exists.');
            return $this->redirectToRoute('app_dashboard');
        }

        // Créer une nouvelle entité Fichiers
        $fileEntity = new Fichiers();
        $fileEntity->setNomFichier($filename);
        $fileSize = filesize($this->getParameter('upload_directory') . '/' . $userId . '/' . $filename);
        $fileSizeInMB = $fileSize/1000000;
        $fileEntity->setTailleFichier($fileSizeInMB); // Taille du fichier
        $fileEntity->setDateUpload(new \DateTime());

        // Récupérer le stockage associé à l'utilisateur connecté
        $stockage = $this->stockagesRepository->findOneBy(['IdUtilisateur' => $user]);

        // Vérifier si un stockage a été trouvé
        if ($stockage) {
            $fileEntity->setIdStockages($stockage);
        } else {
            // Gérer le cas où aucun stockage n'est trouvé pour l'utilisateur connecté
            $this->addFlash('error', 'No storage found for the current user.');
            return $this->redirectToRoute('app_dashboard');
        }

        // Persist et flush les changements
        $this->entityManager->persist($fileEntity);
        $this->entityManager->flush();

        // Ajouter un message de succès
        $this->addFlash('success', 'File uploaded successfully.');

        return $this->redirectToRoute('app_dashboard');
    }


    #[Route('/dashboard/delete-file/{id}', name: 'app_dashboard_delete_file', methods: ['POST'])]
    public function deleteFile(Request $request, Fichiers $file): Response
    {
        $entityManager = $this->entityManager; // Utiliser l'entity manager injecté

        // Récupérer le nom du fichier
        $fileName = $file->getNomFichier();

        // Récupérer l'utilisateur associé au fichier
        $user = $file->getIdStockages()->getIdUtilisateur();

        // Vérifier si l'utilisateur est connecté et si son ID correspond à celui du propriétaire du fichier
        if (!$this->getUser() || $this->getUser()->getId() !== $user->getId()) {
            $this->addFlash('error', 'Unauthorized access.');
            return $this->redirectToRoute('app_dashboard');
        }

        // Supprimer le fichier de la base de données
        $entityManager->remove($file);
        $entityManager->flush();

        // Supprimer le fichier du système de fichiers
        $userId = $user->getId();
        $filePath = $this->getParameter('upload_directory') . '/' . $userId . '/' . $fileName;
        if (file_exists($filePath)) {
            unlink($filePath);
        }

        // Ajouter un message de succès
        $this->addFlash('success', 'File deleted successfully.');

        return $this->redirectToRoute('app_dashboard');
    }

    #[Route('/dashboard/download-file/{id}', name: 'app_dashboard_download_file', methods: ['GET'])]
    public function downloadFile(Request $request, Fichiers $file): Response
    {
        // Récupérer l'utilisateur associé au fichier
        $user = $file->getIdStockages()->getIdUtilisateur();

        // Vérifier si l'utilisateur est connecté et si son ID correspond à celui du propriétaire du fichier
        if (!$this->getUser() || $this->getUser()->getId() !== $user->getId()) {
            $this->addFlash('error', 'Unauthorized access.');
            return $this->redirectToRoute('app_dashboard');
        }

        // Récupérer le nom du fichier
        $fileName = $file->getNomFichier();

        // Récupérer le chemin complet du fichier
        $filePath = $this->getParameter('upload_directory') . '/' . $user->getId() . '/' . $fileName;

        // Vérifier si le fichier existe
        if (!file_exists($filePath)) {
            $this->addFlash('error', 'File not found.');
            return $this->redirectToRoute('app_dashboard');
        }

        // Créer une réponse avec le fichier
        $response = new BinaryFileResponse($filePath);

        // Définir le type de contenu
        $response->headers->set('Content-Type', 'application/octet-stream');

        // Télécharger le fichier au lieu de l'afficher dans le navigateur
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fileName);

        return $response;
    }

}