<?php

namespace App\Controller;

use App\Entity\Factures;
use App\Entity\Utilisateurs;
use App\Form\RegistrationFormType;
use App\Security\EmailVerifier;
use App\Security\UtilisateursAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use App\Entity\Stockages;

class RegistrationController extends AbstractController
{
    private EmailVerifier $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    #[Route('/register', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, UserAuthenticatorInterface $userAuthenticator, UtilisateursAuthenticator $authenticator, EntityManagerInterface $entityManager): Response
    {
        $user = new Utilisateurs();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Encodez le mot de passe
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            // Créez un nouveau stockage pour l'utilisateur
            $stockage = new Stockages();
            $stockage->setIdUtilisateur($user);
            $stockage->setEspaceStockage(2); // Définir l'espace de stockage à 20000 MB
            $stockage->setDateAchat(new \DateTime()); // Définir la date d'achat à la date actuelle

            // Créez une nouvelle facture pour l'utilisateur
            $facture = new Factures();
            $facture->setIdUtilisateur($user);
            $facture->setDateFacture(new \DateTime()); // Définir la date de la facture à la date actuelle
            $facture->setDesignationFacture("Création de l'espace de Stockage ( 20GB )");
            $facture->setPrix(1);
            $facture->setQuantite(2);
            $facture->setPrixTotal(000.2);
            $facture->setMontantTVA(0.20);

            // Persistez les entités
            $entityManager->persist($user);
            $entityManager->persist($stockage);
            $entityManager->persist($facture);
            $entityManager->flush();

            // Générer une URL signée et l'envoyer par e-mail à l'utilisateur
            $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                (new TemplatedEmail())
                    ->from(new Address('sacha.beliot.pro@gmail.com', 'Register Bot'))
                    ->to($user->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );
            // Effectuez d'autres actions nécessaires ici, comme envoyer un e-mail

            return $userAuthenticator->authenticateUser(
                $user,
                $authenticator,
                $request
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form,
        ]);
    }

    #[Route('/verify/email', name: 'app_verify_email')]
    public function verifyUserEmail(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $this->getUser());
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());

            return $this->redirectToRoute('app_register');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'Your email address has been verified.');

        return $this->redirectToRoute('app_dashboard');
    }
}
