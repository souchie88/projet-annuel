<?php

namespace App\Controller;

use App\Entity\Factures;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class FacturesClientController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/factures/client', name: 'app_factures_client')]
    public function index(): Response
    {
        // Récupérer l'utilisateur connecté
        $user = $this->getUser();

        // Récupérer les factures de l'utilisateur connecté
        $factures = $this->entityManager->getRepository(Factures::class)->findBy(['IdUtilisateur' => $user]);

        // Préparer un tableau pour stocker les informations des utilisateurs associées à chaque facture
        $userInfo = [];

        // Parcourir les factures pour récupérer les informations des utilisateurs associés
        foreach ($factures as $facture) {
            // Récupérer l'utilisateur associé à la facture
            $utilisateur = $facture->getIdUtilisateur();

            // Stocker les informations dans le tableau
            $userInfo[$facture->getId()] = [
                'rueClient' => $utilisateur->getRueClient(),
                'villeClient' => $utilisateur->getVilleClient(),
                'codePostalClient' => $utilisateur->getCodePostalClient(),
            ];
        }

        return $this->render('factures_client/index.html.twig', [
            'controller_name' => 'FacturesClientController',
            'factures' => $factures,
            'userInfo' => $userInfo,
        ]);
    }
}
