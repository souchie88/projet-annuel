<?php

namespace App\Controller;

use App\Entity\Stockages;
use App\Form\StockagesType;
use App\Repository\StockagesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/stockages')]
class StockagesController extends AbstractController
{
    #[Route('/', name: 'app_stockages_index', methods: ['GET'])]
    public function index(StockagesRepository $stockagesRepository): Response
    {
        return $this->render('stockages/index.html.twig', [
            'stockages' => $stockagesRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_stockages_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $stockage = new Stockages();
        $form = $this->createForm(StockagesType::class, $stockage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($stockage);
            $entityManager->flush();

            return $this->redirectToRoute('app_stockages_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('stockages/new.html.twig', [
            'stockage' => $stockage,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_stockages_show', methods: ['GET'])]
    public function show(Stockages $stockage): Response
    {
        return $this->render('stockages/show.html.twig', [
            'stockage' => $stockage,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_stockages_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Stockages $stockage, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(StockagesType::class, $stockage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_stockages_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('stockages/edit.html.twig', [
            'stockage' => $stockage,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_stockages_delete', methods: ['POST'])]
    public function delete(Request $request, Stockages $stockage, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$stockage->getId(), $request->request->get('_token'))) {
            $entityManager->remove($stockage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_stockages_index', [], Response::HTTP_SEE_OTHER);
    }
}
